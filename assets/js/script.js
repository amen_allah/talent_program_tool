/**
 *
 * @package   ECHO
 * @theme     Saison Bleue
 * @copyright Copyright (c) 2018 ECHO (http://www.echo.tn)
 *
 */

var DEBUG = true;

$(document).ready(function () {
    if (!DEBUG) {
        document.oncontextmenu = function () {
            return false;
        };
    }

    $(document).on("click", ".action", function (e) {
        e.preventDefault();
        doAction($(this).data(), $(this));
    });

    formulaire = $("#formulaire");
    formulaire.validate({
        ignore: [],
        rules: {
            "fname": {
                required : true
            },
            "lname": {
                required: true
            },
            "mail":{
                required : true,
                email: true
            },
            "about":{
                required : true
            }
        },
        messages: {
            fname: "Please specify your first name",
            lname: "Please specify your last name",
            mail: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            },
            about: "plaease enter your message here"
        }
    });




});



var  doAction = function(d, $e){
        switch (d.action) {
            case "scrollTop":
                $("html, body").animate({
                    scrollTop: 0
                }, 1000);
                break;
        }
    },

    resizeCallback = function(){
        var _h = $(window).height(),
            _w = $(window).width();

    },
    isMediaBreakpointDown = function(mbr) {
        return getMediaBreakpointDown() == mbr;
    },
    getMediaBreakpointDown = function() {
        var _w = $(window).width();

        if (_w < 575)  {
            return "xs";
        } else if (_w > 576 && _w < 767){
            return "sm";
        } else if (_w > 768 && _w < 991){
            return "md";
        } else if (_w > 992 && _w < 1199){
            return "lg";
        } else if (_w > 1200){
            return "xl";
        }
    },
    echo_debug = function(log) {
        if (DEBUG)
            console.log(log);
    }

;
