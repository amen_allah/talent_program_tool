
<?php include "_header.php"; ?>
<body class=" Degrade">
<div class="container-fluid w-100">
    <div class="row justify-content-center mt-3">
        <div class="col-sm-8 text-center">
            <h1>
                <span>Bootstrap</span>
                Login & Register Forms
            </h1>
            <p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-5 login p-4">
                <div class="row ">
                    <div class="col-10">
                        <span>Login to our site</span>
                        <p>Enter username and password to log on:</p>
                    </div>
                    <div class="col-sm-2 align-self-center">
                        <img src="assets/images/padlock.png" class="flaticon">
                    </div>
                </div>
                <form class="form" id="printable">
                    <input class="w-100 mb-3 form-control" type="text" name="username" placeholder="Username...">
                    <input class="w-100 mb-3 form-control" type="password" name="pwd" placeholder="Password...">
                    <button class="btn btn-primary w-100" onclick="myFunction()" type="submit">Sign in!</button>
                </form>
                <div class="rs text-center">
                    <div class="my-3">
                        <span>...Or login with:</span>
                    </div>
                    <ul class="list-inline my-3 d-sm-flex justify-content-center">
                        <li class="list-inline-item">
                            <button class="btn btn-outline-light"><img src="assets/images/facebook-letter-logo.png" class="mr-1">Facebook</button>
                        </li>
                        <li class="list-inline-item">
                            <button class="btn btn-outline-light"><img src="assets/images/twitter.png"class="mr-1"> Twitter</button>
                        </li>
                        <li class="list-inline-item">
                            <button class="btn btn-outline-light"><img src="assets/images/google-plus-logo.png"class="mr-1">Google Plus</button>
                        </li>
                    </ul>
                </div>
            </div>
            <hr class="vertical align-self-center">
            <div class="col-sm-5 login p-4">
                <div class="row ">
                    <div class="col-10">
                        <span>Sign up now</span>
                        <p>Fill in the form below to get instant access</p>
                    </div>
                    <div class="col-sm-2 align-self-center">
                        <img src="assets/images/edit.png" class="flaticon">
                    </div>
                </div>
                <form class="form" id="formulaire" action="">
                    <input class="w-100 mb-3 form-control" type="text" name="fname" placeholder="First name...">
                    <input class="w-100 mb-3 form-control" type="text" name="lname" placeholder="Last name...">
                    <input class="w-100 mb-3 form-control" type="email" name="mail" placeholder="Email...">
                    <textarea class="form-control mb-3" name="about" placeholder="About yourself..."></textarea>
                    <button class="btn btn-primary w-100" type="submit" value="submit">Sign me up!</button>
                </form>
            </div>
        </div>
    </div>
<hr class="horizontal">
</div>
</body>
<?php include "_footer.php";